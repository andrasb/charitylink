
class Config:
	COORDS_PATH = './zipcodecoords.txt'
	GITLAB = {
					"David Vu": {
						"author_name" : ["vudung45", "David Vu"], #account for cases when there are multiple names corresponding to the same member
						"author_id": [2490028],
						"gitlab_username": "davidvu98",
						"picture": 'https:/cdn-images-1.medium.com/max/1600/1*N7iyncKNOj2oqCb2UHwQIQ.jpeg',
						"bio": "Junior, May 2020",
						"position": "Full-stack"

					},
					"Andras Balogh":
					{
						"author_name": ['aroka'],
						"author_id": [2759995],
						"gitlab_username": 'a_roka',
						"picture": 'https:/image.ibb.co/dcDimz/portrait_2_Copy.jpg',
						"bio": "Senior, May 2019",
						"position": "Full-stack"
					},
					"Aitor Cubeles Torres":
					{
						"author_name": ['Aitor C', 'Aitor Cubeles Torres'],
						"author_id": [2772452],
						"gitlab_username": 'aitorct',
						"picture": "https:/cs373-aitor.gitlab.io/blog/img/photo.jpg",
						"bio": "Senior, May 2019",
						"position": "Full-stack"
					},
					"Hassan Syed":
					{
						"author_name" : ["Hassan Syed"],
						"author_id": [],
						"gitlab_username": 'hsyed97',
						"picture": 'https:/i.imgur.com/jEkqoQ6.jpg',
						"bio": "Junior, May 2020",
						"position": "Full-stack"
					},
					"Alexander Strittmatter":
					{
						"author_name" : ["Alsritt", "Alexander Strittmatter", "Alex Strittmatter"],
						"author_id": [2788279],
						"gitlab_username": 'Alsritt',
						"picture": 'https:/i.imgur.com/8zph6mS.jpg',
						"bio": "Senior, May 2019",
						"position": "Full-stack"
					},
					"Thomas Kuhn":
					{
						"author_name" : [],
						"author_id": [2847892],
						"gitlab_username": 't.kuhn',
						"picture": 'https://i.imgur.com/jsfL6T8.jpg',
						"bio": "Senior, May 2019",
						"position": "Full-stack"
					}
			}

	TESTS_URLS = [
						'tests.py',
						'CharityLink.postman_collection.json',
						'../../frontend-react-app/src/App.test.js',
						'../../frontend-react-app/src/guitests.py',
						'../../frontend-react-app/src/views/AboutPage.test.js',
						'../../frontend-react-app/src/views/AboutPerson.test.js',
						'../../frontend-react-app/src/views/CharityBox.test.js',
						'../../frontend-react-app/src/views/RestBox.test.js',
						'../../frontend-react-app/src/views/Search.test.js',
						'../../frontend-react-app/src/views/ui/map.test.js',
						'../../frontend-react-app/src/views/ui/marker.test.js',
						'../../frontend-react-app/src/views/InstancePages/CharityInstance.test.js',
						'../../frontend-react-app/src/views/InstancePages/RestInstance.test.js',
						'../../frontend-react-app/src/views/InstancePages/ZipInstance.test.js',
						'../../frontend-react-app/src/views/visualization/D3Map.test.js',
						'../../frontend-react-app/src/views/SearchBar.test.js'
				]
