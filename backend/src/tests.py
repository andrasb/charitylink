from charity_link_api import app, getLatitudeLongitude
from config import Config
import unittest, json

class BackendTests(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True



    ##########
    # GITLAB #
    ##########

    # AUTHOR: Aitor
    def test_gitlab(self):
        result = self.app.get('/gitlab')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        for member in Config.GITLAB:
            self.assertTrue(member in data["members"])
            self.assertTrue("commits" in data["members"][member])
            self.assertTrue("issues" in data["members"][member])
            self.assertTrue("tests" in data["members"][member])
        self.assertTrue("total_commits" in data)
        self.assertTrue("total_issues" in data)
        self.assertTrue("total_tests" in data)

    # AUTHOR: Aitor
    def test_gitlab_commits(self):
        result = self.app.get('/gitlab/commits')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)

    # AUTHOR: Aitor
    def test_gitlab_commits_by_user(self):
        result = self.app.get('/gitlab/commits/aitorct')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)

    # AUTHOR: Aitor
    def test_gitlab_issues(self):
        result = self.app.get('/gitlab/issues')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)

    # AUTHOR: Aitor
    def test_gitlab_issues_by_user(self):
        result = self.app.get('/gitlab/issues/aitorct')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)

    # AUTHOR: Aitor
    def test_tests(self):
        result = self.app.get('/gitlab/tests')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)

    # AUTHOR: Aitor
    def test_tests_by_user(self):
        result = self.app.get('/gitlab/tests/aitorct')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertTrue("count" in data)



    ##########
    # INCOME #
    ##########

    # AUTHOR: Aitor
    def test_income(self):
        result = self.app.get('/income')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('AMOUNT' in obj)
            self.assertTrue('ZIP' in obj)

    # AUTHOR: Aitor
    def test_income_by_zipcode(self):
        result = self.app.get('/income?zipcode=78751')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        self.assertTrue(len(data) > 0)
        self.assertTrue('AMOUNT' in data[0])
        self.assertTrue('ZIP' in data[0])



    ##############
    # RESTAURANT #
    ##############

    # AUTHOR: Aitor
    def test_restaurant(self):
        result = self.app.get('/restaurant')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('address' in obj)
            self.assertTrue('category' in obj)
            self.assertTrue('city' in obj)
            self.assertTrue('id' in obj)
            self.assertTrue('imageURL' in obj)
            self.assertTrue('latitude' in obj)
            self.assertTrue('longitude' in obj)
            self.assertTrue('name' in obj)
            self.assertTrue('phone' in obj)
            self.assertTrue('price' in obj)
            self.assertTrue('rating' in obj)
            self.assertTrue('state' in obj)
            self.assertTrue('zipCode' in obj)

    # AUTHOR: Aitor
    def test_restaurant_by_zipcode(self):
        result = self.app.get('/restaurant?zipcode=78702')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('address' in obj)
            self.assertTrue('category' in obj)
            self.assertTrue('city' in obj)
            self.assertTrue('id' in obj)
            self.assertTrue('imageURL' in obj)
            self.assertTrue('latitude' in obj)
            self.assertTrue('longitude' in obj)
            self.assertTrue('name' in obj)
            self.assertTrue('phone' in obj)
            self.assertTrue('price' in obj)
            self.assertTrue('rating' in obj)
            self.assertTrue('state' in obj)
            self.assertTrue('zipCode' in obj)

    # AUTHOR: Aitor
    def test_restaurant_by_category(self):
        result = self.app.get('/restaurant?foodType=Italian')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('category' in obj)
            self.assertTrue(obj['category'] == "Italian")

    # AUTHOR: Aitor
    def test_restaurant_by_state(self):
        result = self.app.get('/restaurant?state=TX')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('state' in obj)
            self.assertTrue(obj['state'] == "TX")

    # AUTHOR: Aitor
    def test_restaurant_by_city(self):
        result = self.app.get('/restaurant?city=Austin')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('city' in obj)
            self.assertTrue(obj['city'].lower() == "austin")



    ###########
    # CHARITY #
    ###########

    # AUTHOR: Aitor
    def test_charity(self):
        result = self.app.get('/charity')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('acceptingDonations' in obj)
            self.assertTrue('category' in obj)
            self.assertTrue('city' in obj)
            self.assertTrue('donationURL' in obj)
            self.assertTrue('id' in obj)
            self.assertTrue('latitude' in obj)
            self.assertTrue('longitude' in obj)
            self.assertTrue('missionStatement' in obj)
            self.assertTrue('name' in obj)
            self.assertTrue('state' in obj)
            self.assertTrue('website' in obj)
            self.assertTrue('zipCode' in obj)

    # AUTHOR: Aitor
    def test_charity_by_zipcode(self):
        result = self.app.get('/charity?zipcode=78701')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('acceptingDonations' in obj)
            self.assertTrue('category' in obj)
            self.assertTrue('city' in obj)
            self.assertTrue('donationURL' in obj)
            self.assertTrue('id' in obj)
            self.assertTrue('latitude' in obj)
            self.assertTrue('longitude' in obj)
            self.assertTrue('missionStatement' in obj)
            self.assertTrue('name' in obj)
            self.assertTrue('state' in obj)
            self.assertTrue('website' in obj)
            self.assertTrue('zipCode' in obj)

    # AUTHOR: Aitor
    def test_charity_by_state(self):
        result = self.app.get('/charity?state=TX')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('state' in obj)
            self.assertTrue(obj['state'] == "TX")

    # AUTHOR: Aitor
    def test_charity_by_city(self):
        result = self.app.get('/charity?city=Austin')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), list)
        for obj in data:
            self.assertTrue('city' in obj)
            self.assertTrue(obj['city'].lower() == "austin")


    """
    #########
    # UTILS #
    #########

    # AUTHOR: Aitor
    def test_zipCode_latitude_longitude(self):
        latitude, longitude = getLatitudeLongitude(78751)
        print(latitude, longitude)
        self.assertTrue(latitude)
        self.assertTrue(longitude)

    """


    ##########
    # SEARCH #
    ##########

    # AUTHOR: Aitor
    def test_search(self):
        result = self.app.get('/search?query=hospital')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), dict)
        self.assertTrue('charities' in data)
        self.assertTrue('restaurants' in data)
        self.assertTrue('income' in data)

    # AUTHOR: Aitor
    def test_search_bounds(self):
        result = self.app.get('/search?query=hospital&lower=20000&upper=150000')
        self.assertEqual(result.status_code, 200)
        data = json.loads(result.data)
        self.assertEqual(type(data), dict)
        self.assertTrue('charities' in data)
        self.assertTrue('restaurants' in data)



if __name__ == "__main__":
    unittest.main()
