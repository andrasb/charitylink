import expect from 'expect';
import App from './App.js';
import React from 'react';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests App.js", () => {
  test("Renders without exploding", () => {
  	const wrapper = shallow(<App />);
    expect(wrapper.length).toBe(1);
  });

});