module.exports =
{	
	"host": "http://api.charitylink2.me",
	"endpoints":
	{
		"restaurants": "/restaurant",
		"charities": "/charity",
		"income": "/income",
		"gitlab": "/gitlab",
		"gitlab_tests": "/gitlab/tests"
	},
	members: {
			"David Vu": {
				gitlab_username: "davidvu98",
				picture: 'https://cdn-images-1.medium.com/max/1600/1*N7iyncKNOj2oqCb2UHwQIQ.jpeg',
				bio: "Junior, May 2020",
				position: "Full-stack",
				commits : 0,
				issues: 0

			},
			"Andras Balogh":
			{
				gitlab_username: 'a_roka',
				picture: 'https://image.ibb.co/dcDimz/portrait_2_Copy.jpg',
				bio: "Senior, May 2019",
				position: "Full-stack",
				commits : 0,
				issues: 0
			},
			"Aitor Cubeles Torres":
			{
				gitlab_username: 'aitorct',
				picture: "https://cs373-aitor.gitlab.io/blog/img/photo.jpg",
				bio: "Senior, May 2019",
				position: "Full-stack",
				commits : 0,
				issues: 0
			},
			"Thomas Kuhn":
			{
				gitlab_username: 't.kuhn',
				picture: 'https://i.imgur.com/jsfL6T8.jpg',
				bio: "Senior, May 2019",
				position: "Full-stack",
				commits : 0,
				issues: 0
			},
			"Alexander Strittmatter":
			{
				gitlab_username: 'Alsritt',
				picture: 'https://i.imgur.com/8zph6mS.jpg',
				bio: "Senior, May 2019",
				position: "Full-stack",
				commits : 0,
				issues: 0
			}
	},
	"gitlab_repo": "https://gitlab.com/a_roka/idb9",
	"api_postman": "https://documenter.getpostman.com/view/5482885/RzZAkyHi"
}