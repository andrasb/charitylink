
export default{	
   
	highlight(string, key){
		if(key == "" || key == null)
			return string
	  	return string.replace(new RegExp(key, 'gi'), str => `<mark>${str}</mark>`)
	},
	pad(num, size) {
	    var s = num+"";
	    while (s.length < size) s = "0" + s;
	    return s;
	}

}