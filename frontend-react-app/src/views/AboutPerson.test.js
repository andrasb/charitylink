import React from 'react';
import AboutPerson from './AboutPerson';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests AboutPerson.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<AboutPerson />);
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
