import expect from 'expect';
import D3MapBarGraph from './D3BarGraph.js';
import React from 'react';
import { shallow } from 'enzyme';

describe("D3BarGraph.js", () => {
  test("Renders without exploding", () => {
  	// AUTHOR: David
  	var wrapper = shallow(<D3MapBarGraph type="chvolunteer" />);
    expect(wrapper.length).toBe(1);
    // AUTHOR: David
    wrapper = shallow(<D3MapBarGraph type="chfoodpantry" />);
    expect(wrapper.length).toBe(1);
  });

});