import React from 'react';
import Search from './Search.js';
import expect from 'expect';
import { shallow } from 'enzyme';

describe("Tests Search.js", () => {
   	test("Renders without exploding", () => {
   	  // AUTHOR: David
      var wrapper = shallow(<Search model="restaurants"/>);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      // AUTHOR: David
      wrapper = shallow(<Search model="charities"/>);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      // AUTHOR: David
      wrapper = shallow(<Search model="income"/>);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
    });
  
  });
