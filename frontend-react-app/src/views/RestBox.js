import React from 'react';
import jumbotron from '../css/jumbotron.css'
import utils from '../utils/utils.js'
/* Restaurant box */
class RBox extends React.Component{
	constructor(props){
		super(props);
		
		this.state = {
			modalIsOpen : false
		}
	}


	render(){
		return (
			<div style={{marginTop: "20px"}} className='col-md-3 col-md-offset-3 hover-box'>
				<div className='card' style={{minHeight:"100%"}}>
					<div onClick={() => this.props.relation(this.props.id)}>
					<div className='card-img-top'>
				    	<img  style={{width: '100%', objectFit: 'cover', height: '25vh'}} src={this.props.imageURL} onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}}/>
				    </div>
				      	<div className="card-body">
				      		<h4 dangerouslySetInnerHTML={{__html : utils.highlight(this.props.name,this.props.query)}}></h4>
						    <ul>
						    	 <li dangerouslySetInnerHTML={{__html : "Rating: "+utils.highlight(""+this.props.rating,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Address: "+utils.highlight(this.props.address,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "City: "+utils.highlight(this.props.city,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "State: "+utils.highlight(this.props.state,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Food Type: "+utils.highlight(this.props.category,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Zipcode: "+utils.highlight(""+this.props.zipCode,this.props.query)}}></li>
			      			</ul>
				      		
		      			</div>
		      		</div>
      			</div>
	    	</div>
      );
	}
}

export default RBox;
