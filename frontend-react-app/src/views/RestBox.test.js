import React from 'react';
import RBox from './RestBox';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests RestBox.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<RBox />);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
