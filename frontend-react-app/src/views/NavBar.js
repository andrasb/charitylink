import React from 'react';
import { Link } from 'react-router-dom';

class NavBar extends React.Component {
	constructor(props)
	{
		super(props);

		this.state = {
				currentPage : "Home",
				tabs: this.props.tabs
		}
		this.getCurrentPage = this.getCurrentPage.bind(this);
		this.searchKey = ""

		this.tabChange = this.tabChange.bind(this);
	}

	tabChange(tabNames)
	{
		this.state.currentPage = tabNames;
	}

	componentDidMount()
	{
		this.state.currentPage = this.getCurrentPage();
		this.setState(this.state);
	}

	getCurrentPage()
	{
		let pathname = window.location.pathname
		for(let tab in this.state.tabs)
		{
			let path = this.state.tabs[tab].path.substring(1);
			if(pathname.includes(path) && path != "")
			{
				return tab
			}
			if(pathname.includes("Visualizations"))
				return "Visualizations"
		}
			return "Home"
	}



	render()
	{
		var tabs = []
		var i = 0
		for(var page in this.state.tabs)
		{
			tabs.push(<li className={page == this.state.currentPage ? 'nav-item active': 'nav-item'} key={i}>
			            <Link to={this.state.tabs[page].path} className='nav-link' onClick={this.tabChange.bind(this, page)} key={i}>{page}</Link>
			          </li>);
			++i;
		}
		return (
			 <nav className='navbar navbar-expand-md navbar-dark fixed-top bg-dark'>
			      <Link to='/' className='navbar-brand' onClick={() => this.tabChange("Home")}>CharityLink</Link>
			      <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarsExampleDefault' aria-controls='navbarsExampleDefault' aria-expanded='false' aria-label='Toggle navigation'>
			        <span className='navbar-toggler-icon'>/</span>
			      </button>
			      <div className='collapse navbar-collapse' id='navbarsExampleDefault'>
			        <ul className='navbar-nav mr-auto'>
			        	{tabs}
			        	<li className='nav-item dropdown'>
				            <a className='nav-link dropdown-toggle' id='Visualizations' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Visualizations</a>
				            <div className='dropdown-menu' aria-labelledby='Visualizations'>
				              <Link className='dropdown-item' to='/Visualizations/RestaurantD3' onClick={() => this.tabChange("Visualizations")}>Restaurant</Link>
				        	  <Link className='dropdown-item' to='/Visualizations/CharityD3' onClick={() => this.tabChange("Visualizations")}>Charity</Link>
				        	  <Link  className='dropdown-item'to='/Visualizations/IncomeD3' onClick={() => this.tabChange("Visualizations")}>Income</Link>
				        	  <Link className='dropdown-item' to='/Visualizations/CHVolunteerD3' onClick={() => this.tabChange("Visualizations")}>CH Volunteers</Link>
			        	  	  <Link className='dropdown-item' to='/Visualizations/CHFoodPantryD3' onClick={() => this.tabChange("Visualizations")}>CH Food Pantries</Link>
			        	      <Link  className='dropdown-item'to='/Visualizations/CHStatsD3'  onClick={() => this.tabChange("Visualizations")}>CH States</Link>
				            </div>
			          	</li>
			        </ul>
			        <div className='form-inline my-2 my-lg-0'>
			          <input className='form-control mr-sm-2' type='text' placeholder='Search' aria-label='Search' onChange={(e) => this.searchKey = e.target.value}></input>
			          <button className='btn btn-outline-success my-2 my-sm-0' type='submit' onClick={() => this.props.handleClick("/search/"+this.searchKey)}>Link Me</button>
			        </div>
			      </div>
	    	</nav>
	    )
	}
}

export default NavBar;
