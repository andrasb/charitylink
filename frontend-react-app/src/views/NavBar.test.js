import React from 'react';
import NavBar from './NavBar';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests NavBar.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<NavBar />);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
