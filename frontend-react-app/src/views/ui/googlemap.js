import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import MyGreatPlace from './marker.js';
const AnyReactComponent = ({ text }) => <div>{text}</div>;
 
class GoogleMap extends Component {

  constructor(props)
  {
    super(props);
  }



  render() {
    let center = {
        lat: this.props.latitude,
        lng: this.props.longitude
    }
    console.log(center)
    return (
      // Important! Always set the container height explicitly
      <div style={{height: this.props.height, width: '100%'}}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyBGn4yiYN28XAAEPsFsvpCKNLyJgxfFllI'}}
          defaultCenter={center}
          defaultZoom={11}
        >
         <MyGreatPlace lat={center.lat}
            lng={center.lng}
            text={this.props.name}
            text={''} /* road circle */   />
        </GoogleMapReact>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGn4yiYN28XAAEPsFsvpCKNLyJgxfFllI&callback=initMap">
      </script>
      </div>
    );
  }
}
 
export default GoogleMap;