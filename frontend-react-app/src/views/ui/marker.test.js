import React from 'react';
import MyGreatPlace from './marker';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests MyGreatPlace.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<MyGreatPlace />);

      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();

    });

  });
