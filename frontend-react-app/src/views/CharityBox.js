import React from 'react';
import SimpleMap from './ui/map.js'
import jumbotron from '../css/jumbotron.css'
import utils from '../utils/utils.js'

/* Restaurant box */
class CBox extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			modalIsOpen : false
		}
		this.openModal = this.openModal.bind(this);
		this.afterOpenModal = this.afterOpenModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	openModal() {
		this.setState({modalIsOpen: true});
	}

	afterOpenModal() {
	// references are now sync'd and can be accessed.
		//this.subtitle.style.color = '#f00';
	}

	closeModal() {
		this.setState({modalIsOpen: false});
	}

	render(){
		return (
			<div style={{marginTop: "20px"}} className='col-md-3 col-md-offset-3 hover-box'>
				<div className='card' style={{minHeight:"100%"}}>
					<div onClick={() => this.props.relation(this.props.id)}>
						
				      	<div className="card-body">
				      		<h4 dangerouslySetInnerHTML={{__html : utils.highlight(this.props.name,this.props.query)}}></h4>
						    <ul>
						         <li dangerouslySetInnerHTML={{__html : "City: "+utils.highlight(this.props.city,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "State: "+utils.highlight(this.props.state,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Zipcode: "+utils.highlight(""+this.props.zipCode,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Accepting Donation: "+utils.highlight(this.props.acceptingDonations == 1 ? "Yes" : "No" ,this.props.query)}}></li>
						         <li dangerouslySetInnerHTML={{__html : "Mission Statement: "+utils.highlight(this.props.missionStatement == null ? "" : this.props.missionStatement,this.props.query)}}></li>
			      			</ul>
				      		
		      			</div>
	      			</div>
	      		</div>
	    	</div>
      );
	}
}

export default CBox;
