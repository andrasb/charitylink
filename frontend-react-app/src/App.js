import React from 'react';
import './css/App.css';
import './css/jumbotron.css'
import NavBar from './views/NavBar.js'
import AboutPage from './views/AboutPage.js'
import SearchBar from './views/SearchBar.js'
import Search from './views/Search.js'
import RestInstance from './views/InstancePages/RestInstance.js'
import CharityInstance from './views/InstancePages/CharityInstance.js'
import ZipInstance from './views/InstancePages/ZipInstance.js'
import D3Map from './views/visualization/D3Map.js'
import D3BarGraph from './views/visualization/D3BarGraph.js'
import { Link, Switch, Route ,withRouter, Redirect } from 'react-router-dom';

class Splash extends React.Component{
	constructor(props)
	{
		super(props);
		
	}

	render()
	{
		return (
			<div className="jumbotron">
		        <div className="container">
		          <h1 className="display-3">Charity Link</h1>
		          <p>Redistribution of resources is a civic difficulty that plagues almost any aspect of charity, be it receiving, gifting, or distributing. CharityLink is a platform designed to give restaurant and food distributors information on local charities, and give charities an effective way to find the most in-need locations to distribute the donated food. Also, this gives individual users the ability to find charitable small businesses or volunteer opportunities near them.</p>
		          <p><Link to='/about' className="btn btn-primary btn-lg" >Learn more &raquo;</Link></p>
		        </div>
	        </div>
      )
	}

}

class App extends React.Component {
  	constructor(props)
	{
		super(props);
		

		this.handleClick = this.handleClick.bind(this);

		this.components =
		{
			"Home": <Splash />,
			"Search":<SearchBar />,
			"About Page":  <AboutPage />,
			"Restaurants": <Search handleClick={this.handleClick} model="restaurants" key={0}/>,
			"Charities": <Search handleClick={this.handleClick} model="charities" key={1}/>,
			"Income":  <Search handleClick={this.handleClick} model="income" key={1}/>
		}
		this.state = {
			redirect : null
		}

		this.tabs = {
			"Home": {
				"path": "/",
				"route": <Route exact path='/' render={() => this.components["Home"]} key={0} {...this.props}/>
			},
			"Find Restaurants": {
				"path" : "/restaurant",
				"route": <Route path='/restaurant' render={() => this.components["Restaurants"]} key={1} />
			},

			"Find Charities": {
				"path" : "/charity",
				"route": <Route path='/charity'  render={() => this.components["Charities"]} key={2} {...this.props}/>
			},
			"Find By Zip/Income": {
				"path" : "/income",
				"route": <Route path='/income'  render={() => this.components["Income"]} key={4} />
			},
			"About Page": {
				"path" : "/about",
				"route": <Route path='/about'  render={() => this.components["About Page"]} key={3} {...this.props}/>
			}
		}
		
	}

	handleClick(path) {
		this.setState({redirect : path})
	}

	render()
	{
		let routes = [];
		for(var page in this.tabs)
		{
			routes.push(this.tabs[page].route);
		}

		if(this.state.redirect != null)
		{
			let reditect = this.state.redirect;
			this.state.redirect = null;
			return (
                <Redirect to={reditect} push />
            );
		}

		return (
			<div>
				 	<NavBar tabs={this.tabs} handleClick={this.handleClick}/>
				 	<Switch>
				 		{routes}
				 		<Route path='/restinfo/:id' render={(props) => <RestInstance {...props} handleClick={this.handleClick} />}/>
				 		<Route path='/charityinfo/:id' component={CharityInstance}/>
				 		<Route path='/zipinfo/:id' component={ZipInstance}/>
				 		<Route path='/Visualizations/RestaurantD3' render={(props) => <D3Map {...props} key="1" type="restaurants" handleClick={this.handleClick} />}/>
				 		<Route path='/Visualizations/CharityD3' render={(props) => <D3Map {...props} key="2" type="charities" handleClick={this.handleClick} />}/>
				 		<Route path='/Visualizations/IncomeD3' render={(props) => <D3Map {...props} key="3" type="income" handleClick={this.handleClick} />}/>
				 		<Route path='/Visualizations/CHVolunteerD3' render={(props) => <D3BarGraph {...props} key="4" type="chvolunteer" handleClick={this.handleClick} />}/>
				 		<Route path='/Visualizations/CHFoodPantryD3' render={(props) => <D3BarGraph {...props} key="5" type="chfoodpantry" handleClick={this.handleClick} />}/>
				 		<Route path='/Visualizations/CHStatsD3' render={(props) => <D3Map {...props} key="6" type="chstats" handleClick={this.handleClick} />}/>
				 		<Route path='/Search/:query' render={(props) => <SearchBar {...props} handleClick={this.handleClick} />}/>
				 	</Switch>
				
			</div>

	    );
	}
}

export default App;
